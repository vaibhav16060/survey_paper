all: paper.pdf

paper.pdf: $(wildcard *.tex) paper.bib
	pdflatex paper.tex
	bibtex paper
	pdflatex paper.tex
	pdflatex paper.tex

clean:
	-rm -f *.aux
	-rm -f *.bbl
	-rm -f *.blg
	-rm -f *.log
	-rm -f *.out
	-rm -f *.pdf
